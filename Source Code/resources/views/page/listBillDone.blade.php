@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">
<div id="search">
    <form action="findBillDone" method="get">
        <input type="text" class="text" name="idBill" id="txtSearch" placeholder="Nhập ID" style="width: 250px;"/>
        <button type="submit">Tìm kiếm</button>
    </form>
</div>
<h6>{{$error ?? ''}}</h6>
<div class="table-title text-center">
    <h4>Danh sách hóa đơn</h4>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID hóa đơn</th>
            <th class="text-left">ID nhân viên</th>
            <th class="text-left">Tổng tiền</th>
            <th class="text-left">Ngày thanh toán</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('individual_BillDone','idBill='.$da['idBill'])}}">{{$da['idBill']}}</a></td>
            <td class="text-left"><a href="{{route('individual_BillDone','idBill='.$da['idBill'])}}">{{$da['staff']}}</a></td>
            <td class="text-left"><a href="{{route('individual_BillDone','idBill='.$da['idBill'])}}">{{$da['sumFinal']}}</a></td>
            <td class="text-left"><a href="{{route('individual_BillDone','idBill='.$da['idBill'])}}">{{$da['date']}}</a></td>
        </tr>
@endforeach         
    </tbody>
</table>

<!-- <script src="js/staffMN.js"></script> -->
@endsection