@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/avt.png')}}">
    <h3>{{$da['staffId']}}- {{$da['name']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">Nhập mật khẩu cũ</td>
            <td class="text-left"><input id='passOld' type="password" name="passOld"></td>
        </tr>
        <tr>
            <td class="text-left">Nhập mật khẩu mới</td>
            <td class="text-left"><input id='passNew' type="password" name="passNew"></td>
        </tr>
        <input type="hidden" id='pass' value="{{$da['pass']}}">
    </tbody>
</table>
<div class="table-title text-center">
    <button id="save" class="btn btn-success">Đổi</button>
</div>
<div id='error' class='text-center'></div>
<div id='mess' class='text-center'></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/changePass.js"></script>
@endforeach
@endsection