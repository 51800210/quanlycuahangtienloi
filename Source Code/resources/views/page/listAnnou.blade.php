@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">

<div class="table-title text-center">
    <h4>Danh sách thông báo</h4>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID người gửi</th>
            <th class="text-left">Họ & tên người gửi</th>
            <th class="text-left">Thời gian</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('Annou','ID='.$da['_id'])}}">{{$da['idSent']}}</a></td>
            <td class="text-left"><a href="{{route('Annou','ID='.$da['_id'])}}">{{$da['nameSent']}}</a></td>
            <td class="text-left"><a href="{{route('Annou','ID='.$da['_id'])}}">{{$da['date']}}</a></td>
        </tr>
@endforeach         
    </tbody>
</table>



<!-- <script src="js/staffMN.js"></script> -->

@endsection