<!DOCTYPE html>
<html lang="en">
<head>
	 <link rel="stylesheet" href="css/Login.css">
</head>
<body>
	<div class="loginbox">
	<img src="img/login.jpg" class="avatar">	
		<h1> LOGIN </h1>  		
		<form action="{{route('processLogin')}}" method="post">
            {{csrf_field()}}
			<p> Username </p>
			<input type="text" name="staffId" placeholder="Enter username">
			<p> Password </p>
			<input type="password" name="pass" placeholder="Enter password">
			<input type="submit" name="" value="Login">
            {{$err ?? '' }}          
		</form>
	</div>
</body>