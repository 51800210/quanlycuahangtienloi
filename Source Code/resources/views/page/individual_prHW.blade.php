@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/OIP.jpg')}}">
    <h3>{{$da['ID']}}- {{$da['name']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td id='ID' class="text-left">{{$da['ID']}}</td>
        </tr>
        <tr>
            <td class="text-left">Loại sản phẩm</td>
            <td id='ID' class="text-left">{{$da['type']}}</td>
        </tr>
        <tr>
            <td class="text-left">tên sản phẩm</td>
            <td id='ID' class="text-left">{{$da['name']}}</td>
        </tr>
        <tr>
            <td class="text-left">Giá</td>
            <td id='ID' class="text-left">{{$da['price']}}</td>
        </tr>
        <tr>
            <td class="text-left">Số lượng</td>
            <td id='ID' class="text-left">{{$da['qty']}}</td>
        </tr>
        <tr>
            <td class="text-left">Ngày nhập kho</td>
            <td id='ID' class="text-left">{{$da['date']}}</td>
        </tr>
        <tr>
            <td class="text-left">HSD</td>
            <td id='ID' class="text-left">{{$da['HSD']}}</td>
        </tr>
        <tr>
            <td class="text-left">Nhà cung cấp</td>
            <td id='ID' class="text-left">{{$da['NCC']}}</td>
        </tr>
        <tr>
            <td class="text-left">Nhân viên nhập hàng</td>
            <td id='ID' class="text-left">{{$da['staff_inPut']}}</td>
        </tr>
    </tbody>
</table>
<div class="table-title text-center">
    <button class="btn btn-basic"><a href="{{route('listProduct')}}">Quay lại</a></button>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@endforeach
@endsection