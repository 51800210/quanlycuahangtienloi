@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">


<div class="table-title text-center">
    <h4>Danh sách sản phẩm hết hạn</h4>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID sản phẩm</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Loại</th>
            <th class="text-left">Số lượng</th>
            <th class="text-left">Nhân viên thêm</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left">{{$da['ID']}}</td>
            <td class="text-left">{{$da['name']}}</td>
@if($da['type'] == 1)
            <td class="text-left">Đồ ăn</td>
@elseif($da['type'] == 2)
            <td class="text-left">Đồ uống</td>
@else
            <td class="text-left">Đồ dùng</td>
@endif
            <td class="text-left">{{$da['qty']}}</td>
            <td class="text-left">{{$da['staff_inPut']}}</td>

        </tr>
@endforeach         
    </tbody>
</table>



<script src="js/staffMN.js"></script>

@endsection