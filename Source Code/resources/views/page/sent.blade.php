@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/sent.css">

<h1>Thông báo</h1>
<form onsubmit="return checksb()" action="{{route('prSent')}}" method="get">
    <div class="content">Chọn đối tượng</div>
    <textarea id="staffId" name="staffId" rows="1" cols="150" placeholder="Nhiều nhân viên cách nhau dấu ','"></textarea>
    <input type="checkbox" id="all" name="all" value="all">Gửi tất cả
    
    <div class="content">Nội dung tin nhắn</div>
    <textarea id="contenttb" name="content" rows="12" cols="150" placeholder="Viết tin nhắn tại đây"></textarea>

    <div id="error">{{$error ?? ''}}</div>
    <div id="success">{{$success ?? ''}}</div>
    <input type="hidden" id="date" name="date">
    <div class="text-center">
        <button id="sent" class="btn btn-info" type="submit">Gửi</button>
        <button id="cancel" class="btn btn-danger" type="button">Hủy</button>
    </div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/sent.js"></script>
@endsection