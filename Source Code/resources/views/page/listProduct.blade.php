@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">

<div id="search">
    <form action="findPrHW" method="get">
        <input type="text" class="txt" name="ID" id="txtSearch" placeholder="Nhập ID" style="width: 250px;" />
        <button type="submit">Tìm kiếm</button>
    </form>
    <h6>{{$error ?? ''}}</h6>
</div>

<div class="table-title text-center">
    <h4>Danh sách sản phẩm trong kho</h4>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID sản phẩm</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('individual_prHW','ID='.$da['ID'])}}">{{$da['ID']}}</a></td>
            <td class="text-left"><a href="{{route('individual_prHW','ID='.$da['ID'])}}">{{$da['name']}}</a></td>
            <td class="text-left"><a href="{{route('individual_prHW','ID='.$da['ID'])}}">{{$da['price']}}</a></td>
        </tr>
@endforeach         
    </tbody>
</table>

@endsection