@extends('master.AdInterface')
@section('content') 

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel='stylesheet' type='text/css' href='css/invoice.css' />
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
</head>
		<h5 id='success'>{!!$success!!}</h5>
		<h5 id='error'>{!!$error!!}</h5>

		<textarea id="header">HÓA ĐƠN</textarea>
		
		<div id="identity">
		
            <div id="address">
				<p>D/c: Q.7,TP.HCM</p>
				<p>Phone: (555) 555-5555</p>
			</div>

			
		
		</div>
		
		<div style="clear:both"></div>
<form onsubmit="return checksb()" action="{{route('addBill')}}" method ='post'>
	{{csrf_field()}}		
		<div id="customer">
            <table id="meta">
                <tr>
                    <td class="meta-head">Mã đơn hàng</td>
                    <td><textarea id="idBill" name="idBill">000123</textarea></td>
                </tr>
                <tr>
                    <td class="meta-head">Ngày</td>
                    <td><textarea id="date" name="date"></textarea></td>
                </tr>
				<tr>
                    <td class="meta-head">Nhân viên</td>
                    <td>{{$name}}</td>
                </tr>
				<tr>
                    <td class="meta-head">Phương thức</td>
                    <td>
						<div class="form-group">
							<select id="PT" name="PT">
								<option value="0">Tiền mặt</option>
								<option value="1">Thẻ</option>
							</select>
						</div>
					</td>
                </tr>

				<tr>
                    <td class="meta-head">Thẻ thành viên</td>
					<td><textarea id="Cus" name="Cus"></textarea></td>
                </tr>
				<tr>
                    <td  class="meta-head">Thành tiền</td>
                    <td><textarea id='sumFinal' class="due" name="sumFinal"></textarea></td>
                </tr>
            </table>		
		</div>
		<div class="text-right" id="gr-btn">
		<button type="submit" class="btn btn-success" id='TT'>Thanh toán</button>
		<button onclick="window.print()" type="button" class="btn btn-success" id='TT'><i class="fa fa-print" aria-hidden="true"></i> In</button>
		</div>
		<table id="items">		
			<tr>
				<th><div id="add" class="delete-wpr"><a class="delete" title="Remove row">+</a>Mã sản phẩm</div></th>
				<th>Tên sản phẩm</th>
				<th>Giá</th>
				<th>Số lượng</th>
				<th>Khuyến mãi</th>
				<th>Tổng</th>
			</tr>

			<tr id= 'SR0' class="item-row">
				<td class="item-name"><div onclick="removeR('SR0')" class="delete-wpr"><a class="delete" title="Remove row">X</a></div><textarea id="ID0" name="ID0" placeholder="Nhập ID sản phẩm"></textarea></td>
				<td><textarea name="productName0" id="productName0" class="description" placeholder="x"></textarea></td>
				<td><textarea name="cost0" id="cost0" class="cost" placeholder="x"></textarea></td>
				<td><textarea onkeyup="findProduct('qt0',0)" id="qt0" name="qt0" placeholder="Số lượng"></textarea></td>
				<td><textarea name="discount0" id="discount0" class="discount" placeholder="x"></textarea></td>
				<td><textarea name="sum0" id="sum0" class="qty" placeholder="x"></textarea></td>
			</tr>  
	
		</table>
		<input type="hidden" id="count" name="count">
</form>
		<div class="text-center" id="gr-btn">
			<button class="btn btn-info" id="confirm">Xác nhận</button>
			<button class="btn btn-danger" id="cancel">Hủy</button>
		</div>
	</div>



    <script src="js/HD.js"></script>
@endsection