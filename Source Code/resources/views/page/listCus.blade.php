@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">

<div id="search">
    <form action="findCus" method="get">
        <input type="text" class="txt" name="ID" id="txtSearch" placeholder="Nhập ID" style="width: 250px;"/>
        <button type="submit">Tìm kiếm</button>
    </form>
</div>
<h6>{{$error ?? ''}}</h6>
<div class="table-title text-center">
    <h4>Danh sách khách hàng</h4>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Họ & tên</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('individual_Cus','ID='.$da['ID'])}}">{{$da['ID']}}</a></td>
            <td class="text-left"><a href="{{route('individual_Cus','ID='.$da['ID'])}}">{{$da['name']}}</a></td>
        </tr>
@endforeach         
    </tbody>
</table>



<!-- <script src="js/staffMN.js"></script> -->

@endsection