@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/avt.png')}}">
    <h3>{{$da['idBill']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID hóa đơn</td>
            <td id='ID' class="text-left">{{$da['idBill']}}</td>
        </tr>
        <tr>
            <td class="text-left">Ngày lập hóa đơn</td>
            <td id='ID' class="text-left">{{$da['date']}}</td>
        </tr>
        <tr>
            <td class="text-left">ID Nhân viên thah toán</td>
            <td id='ID' class="text-left">{{$da['staff']}}</td>
        </tr>
        <tr>
            <td class="text-left">Phương thức thanh toán</td>
@if($da['PT'] == 1)
            <td id='ID' class="text-left">Thẻ</td>
@else
            <td id='ID' class="text-left">Tiền mặt</td>
@endif
        </tr>
        <tr>
            <td class="text-left">Tổng hóa đơn</td>
            <td id='ID' class="text-left">{{$da['idBill']}}</td>
        </tr>
        
    @foreach($da['info'] as $d)
        <tr>
            <td  style="color:red" class="text-left">ID Sản phẩm</td>
            <td id='ID' class="text-left">{{$d['ID']}}</td>
    
        </tr>
        <tr>
            <td class="text-left">Tên sản phẩm</td>
            <td id='ID' class="text-left">{{$d['productName']}}</td>
        </tr>
        <tr>
            <td class="text-left">Số lượng</td>
            <td id='ID' class="text-left">{{$d['qt']}}</td>
        </tr>
        <tr>
            <td class="text-left">Giá/sản phẩm</td>
            <td id='ID' class="text-left">{{$d['cost']}}</td>
        </tr>
        <tr>
            <td class="text-left">Khuyến mãi</td>
            <td id='ID' class="text-left">{{$d['discount']}}</td>
        </tr>
        <tr>
            <td class="text-left">Tổng</td>
            <td id='ID' class="text-left">{{$d['sum']}}</td>
        </tr>

    @endforeach

    </tbody>
</table>

<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>

<div class="table-title text-center">
    <button class="btn btn-basic"><a href="{{route('listBillDone')}}">Quay lại</a></button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- <script src="js/individual_Cus.js"></script>
 -->@endforeach
@endsection