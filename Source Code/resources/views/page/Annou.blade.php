@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/avt.png')}}">
    <h3>{{$da['_id']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td id='ID' class="text-left">Người gửi: {{$da['idSent']}} - {{$da['nameSent']}}</td>
        </tr>
        <tr>
            <td id='ID' class="text-left" style="word-break: break-all;"><b>Nội dung:</b></br> {{$da['content']}}</td>
        </tr>
        <tr>
            <td id='ID' class="text-left">Date: {{$da['date']}}</td>
        </tr>
    </tbody>
</table>

<div class="table-title text-center">
    <button class="btn btn-basic"><a href="{{route('listAnnou')}}">Quay lại</a></button>
</div>


@endforeach
@endsection