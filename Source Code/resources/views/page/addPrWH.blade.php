@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="./css/addCus.css">
<div class="table-title text-center">
    <img id='img' src="{{asset('img/DB.png')}}">
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td class="text-left"><input id='ID' type="text" name="ID"></td>
        </tr>
        <tr>        
            <td class="text-left">Loại sản phẩm</td>
            <td class="text-left">
                <select id="type" name="type">
					<option value="1">Đồ ăn</option>
					<option value="2">Nước</option>
				    <option value="3">Vật dụng</option>
				</select>
            </td>
        </tr>
        <tr>
            <td class="text-left">Tên sản phẩm:</td>
            <td class="text-left"><input id='name' type="text" name="name"></td>
        </tr>
        
        <tr>
            <td class="text-left">Giá Nhập</td>
            <td class="text-left"><input id='priceInput' type="text" name="priceInput"></td>
        </tr>
        <tr>
            <td class="text-left">Giá bán</td>
            <td class="text-left"><input id='price' type="text" name="address"></td>
        </tr>
        <tr>
            <td class="text-left">Số lượng</td>
            <td class="text-left"><input id='qty' type="text" name="qty"></td>
        </tr>
        <tr>
            <td class="text-left">Hạn sử dụng</td>
            <td class="text-left"><input id='HSD' type="text" name="HSD"></td>
        </tr>
        <tr>
            <td class="text-left">Nhà cung cấp</td>
            <td class="text-left"><input id='NCC' type="text" name="NCC"></td>
        </tr>
        <input id='date' type="hidden" name="date">
    </tbody>
</table>
<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>
<div class="table-title text-center">
    <button id="save" class="btn btn-success">Lưu lại</button>
    <button id="new" class="btn btn-basic">Làm mới</button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/addPrWH.js"></script>

@endsection