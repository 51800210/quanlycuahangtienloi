@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/listIO.css">

<div class="text-right">
    <button id="shwIn" class="btn btn-success">Nhập kho</button>
    <button id="shwOut" class="btn btn-warning">Xuất kho</button>
    <button id="shwBill" class="btn btn-danger">Hóa đơn</button>
    <button id="shwHH" class="btn btn-danger">Hàng hết hạng</button>

</div>

<div class="table-title text-center">
    <h4 id="inHW">Danh sách sản phẩm nhập kho</h4>
</div>
<table id="tb-inHW" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Loại</th>
            <th class="text-left">Số lượng</th>
            <th class="text-left">Giá nhập</th>
            <th class="text-left">Ngày</th>
            <th class="text-left">Tổng giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
<?php
    $sum = 0;
?>
@foreach($data as $da)
        <tr>
            <td class="text-left"><a href="#">{{$da['ID']}}</a></td>
            <td class="text-left"><a href="#">{{$da['name']}}</a></td>
    @if($da['type'] == 1)
            <td class="text-left"><a href="#">Đồ ăn</a></td>
    @elseif($da['type'] == 2)
            <td class="text-left"><a href="#">Nước</a></td>
    @else
            <td class="text-left"><a href="#">Đồ dùng</a></td>
    @endif
            <td class="text-left"><a href="#">{{$da['qty']}}</a></td>
            <td class="text-left"><a href="#">{{$da['priceInput']}}</a></td>
            <td class="text-left"><a href="#">{{$da['date']}}</a></td>
            <td class="text-left"><a href="#">{{$da['AllPrice']}}</a></td>
        </tr>
    <?php
        $sum += $da['AllPrice'];
    ?>       
@endforeach 
    </tbody>
</table>

<h4 id="sumIn" class="sum">Tổng: {{$sum}} đ</h4>

<!-- prOut -->
<div class="table-title text-center">
    <h4 id="outHW">Danh sách sản phẩm xuất kho</h4>
</div>
<table id="tb-outHW" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Loại</th>
            <th class="text-left">Số lượng</th>
            <th class="text-left">Giá bán</th>
            <th class="text-left">Ngày</th>
            <th class="text-left">Tổng giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
<?php
    $sum = 0;
?>
@foreach($dataOut as $daOut)
        <tr>
            <td class="text-left"><a href="#">{{$daOut['ID']}}</a></td>
            <td class="text-left"><a href="#">{{$daOut['name']}}</a></td>
    @if($daOut['type'] == 1)
            <td class="text-left"><a href="#">Đồ ăn</a></td>
    @elseif($daOut['type'] == 2)
            <td class="text-left"><a href="#">Nước</a></td>
    @else
            <td class="text-left"><a href="#">Đồ dùng</a></td>
    @endif
            <td class="text-left"><a href="#">{{$daOut['qty']}}</a></td>
            <td class="text-left"><a href="#">{{$daOut['price']}}</a></td>
            <td class="text-left"><a href="#">{{$daOut['date']}}</a></td>
            <td class="text-left"><a href="#">{{$daOut['AllPrice']}}</a></td>
        </tr>
    <?php
        $sum += $da['AllPrice'];
    ?>       
@endforeach 
    </tbody>
</table>
<h4 id="sumOut" class="sum">Tổng: {{$sum}} đ</h4>

<!-- prHH -->

<div class="table-title text-center">
    <h4 id="HH">Danh sách sản phẩm hết hạn</h4>
</div>
<table id="tb-HH" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Loại</th>
            <th class="text-left">Số lượng</th>
            <th class="text-left">Giá nhập</th>
            <th class="text-left">Ngày</th>
            <th class="text-left">Tổng giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
<?php
    $sumHH = 0;
?>
@foreach($dataHH as $daHH)
    <?php
        $all = $daHH['qty'] * $daHH['priceInput'];
        $sumHH += $all;
    ?>
        <tr>
            <td class="text-left"><a href="#">{{$daHH['ID']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['name']}}</a></td>
    @if($daHH['type'] == 1)
            <td class="text-left"><a href="#">Đồ ăn</a></td>
    @elseif($daHH['type'] == 2)
            <td class="text-left"><a href="#">Nước</a></td>
    @else
            <td class="text-left"><a href="#">Đồ dùng</a></td>
    @endif
            <td class="text-left"><a href="#">{{$daHH['qty']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['priceInput']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['date']}}</a></td>
            <td class="text-left"><a href="#">{{$all}}</a></td>
        </tr>       
@endforeach 
    </tbody>
</table>
<h4 id="sumHH" class="sum">Tổng: {{$sumHH}} đ</h4>


<!-- prBill -->
<div class="table-title text-center">
    <h4 id="Bill">Tổng hóa đơn</h4>
</div>
<table id="tb-Bill" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID hóa đơn</th>
            <th class="text-left">Ngày lập</th>
            <th class="text-left">ID Nhân viên</th>
            <th class="text-left">Thanh toán</th>
            <th class="text-left">Số sản phẩm</th>
            <th class="text-left">Tổng giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
<?php
    $sumBill = 0;
?>
@foreach($dataBill as $daBill)
    <?php
        $sumBill += $daBill['sumFinal'];
        $count = count($daBill['info']);
    ?>
        <tr>
            <td class="text-left"><a href="#">{{$daBill['idBill']}}</a></td>
            <td class="text-left"><a href="#">{{$daBill['date']}}</a></td>
            <td class="text-left"><a href="#">{{$daBill['staff']}}</a></td>
    @if($daBill['PT'] == 0)
            <td class="text-left"><a href="#">Tiền mặt</a></td>
    @else
            <td class="text-left"><a href="#">Thẻ</a></td>
    @endif
            <td class="text-left"><a href="#">{{$count}}</a></td>

            <td class="text-left"><a href="#">{{$daBill['sumFinal']}}</a></td>
        </tr>       
@endforeach 
    </tbody>
</table>
<h4 id="sumBill" class="sum">Tổng: {{$sumBill}} đ</h4>

<script src="js/listIO.js"></script>

@endsection