@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="./css/addCus.css">
<div class="table-title text-center">
    <img id='img' src="{{asset('img/addPrStore.jpg')}}">
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td class="text-left"><input id='ID' type="text" name="ID"></td>
        </tr>
        <tr>
            <td class="text-left">Số lượng</td>
            <td class="text-left"><input id='qty' type="text" name="qty"></td>
        </tr>
    </tbody>
</table>
<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>
<div class="table-title text-center">
    <button id="save" class="btn btn-success">Lưu lại</button>
    <button id="new" class="btn btn-basic">Làm mới</button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/addPrStore.js"></script>
@endsection