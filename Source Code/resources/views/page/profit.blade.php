@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/profit.css">


<div class="text-right">
    <button id="shwHH" class="btn btn-success">Lương nhân viên</button>
    <button id="shwSal" class="btn btn-warning">Sản phẩm hết hạn</button>
</div>

<!-- prHH -->

<div class="table-title text-center">
    <h4 id="HH">Danh sách sản phẩm hết hạn</h4>
</div>
<table id="tb-HH" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Tên sản phẩm</th>
            <th class="text-left">Loại</th>
            <th class="text-left">Số lượng</th>
            <th class="text-left">Giá nhập</th>
            <th class="text-left">Ngày</th>
            <th class="text-left">Tổng giá</th>
        </tr>
    </thead>
    <tbody class="table-hover">
<?php
    $sumHH = 0;
    $sumSal = 0;
?>
@foreach($dataHH as $daHH)
    <?php
        $all = $daHH['qty'] * $daHH['priceInput'];
        $sumHH += $all;
    ?>
        <tr>
            <td class="text-left"><a href="#">{{$daHH['ID']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['name']}}</a></td>
    @if($daHH['type'] == 1)
            <td class="text-left"><a href="#">Đồ ăn</a></td>
    @elseif($daHH['type'] == 2)
            <td class="text-left"><a href="#">Nước</a></td>
    @else
            <td class="text-left"><a href="#">Đồ dùng</a></td>
    @endif
            <td class="text-left"><a href="#">{{$daHH['qty']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['priceInput']}}</a></td>
            <td class="text-left"><a href="#">{{$daHH['date']}}</a></td>
            <td class="text-left"><a href="#">{{$all}}</a></td>
        </tr>       
@endforeach 
    </tbody>
</table>
<h4 id="sumHH" class="sum">Tổng: {{$sumHH}} đ</h4>


<!--Staff-->
<div class="table-title text-center">
    <h4 id="Sal">Lương nhân viên</h4>
</div>
<table id="tb-Sal" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Tên nhân viên</th>
            <th class="text-left">Lương</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach($dataStaff as $daStaff)
    <?php
        $sumSal += $daStaff['salery'];
    ?>
        <tr>
            <td class="text-left"><a href="#">{{$daStaff['staffId']}}</a></td>
            <td class="text-left"><a href="#">{{$daStaff['name']}}</a></td>
            <td class="text-left"><a href="#">{{$daStaff['salery']}}</a></td>
        </tr>       
@endforeach 
    </tbody>
</table>
<h4 id="sumSal" class="sum">Tổng: {{$sumSal}} đ</h4>
<script src="js/profit.js"></script>

@endsection