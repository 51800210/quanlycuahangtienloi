@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="./css/addCus.css">
<div class="table-title text-center">
    <img id='img' src="{{asset('img/sale.png')}}">
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID Khuyến mãi</td>
            <td id='ID' class="text-left"></td>
        </tr>

        <tr>
            <td class="text-left">ID sản phẩm</td>
            <td class="text-left"><input id='idPr' type="text" name="idPr"></td>
        </tr>
        <tr>
            <td class="text-left">% khuyến mãi</td>
            <td class="text-left"><input id='sale' type="text" name="sale"></td>
        </tr>
    </tbody>
</table>
<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>
<div class="table-title text-center">
<button id="save" class="btn btn-success">Lưu lại</button>
<button id="new" class="btn btn-basic">Mới</button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/addSale.js"></script>

@endsection