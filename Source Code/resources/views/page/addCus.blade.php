@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="./css/addCus.css">
<div class="table-title text-center">
    <img id='img' src="{{asset('img/addCus.png')}}">
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td id='ID' class="text-left"></td>
        </tr>
        <tr>
            <td class="text-left">Họ & tên</td>
            <td class="text-left"><input id='name' type="text" name="name"></td>
        </tr>
        <tr>        
            <td class="text-left">Giới tính</td>
            <td class="text-left">
                <select id="gender" name="gender">
					<option value="0">Nam</option>
					<option value="1">Nữ</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="text-left">CMND</td>
            <td class="text-left"><input id='CMND' type="text" name="CMND"></td>
        </tr>
        <tr>
            <td class="text-left">Địa chỉ</td>
            <td class="text-left"><input id='address' type="text" name="address"></td>
        </tr>
        <tr>
            <td class="text-left">Số điện thoại</td>
            <td class="text-left"><input id='phone' type="text" name="phone"></td>
        </tr>
    </tbody>
</table>
<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>
<div class="table-title text-center">
    <button id="save" class="btn btn-success">Lưu lại</button>
    <button id="new" class="btn btn-basic">Làm mới</button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/addCus.js"></script>

@endsection