@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/listStaff.css">

<div id="search">
    <form action="findStaff" method="get">
        <input type="text" class="txt" name="ID" id="txtSearch" placeholder="Nhập ID" style="width: 250px;" />
        <button type="submit">Tìm kiếm</button>
	</form>
	<h6>{{$error ?? ''}}</h6>
</div>
<div id="add">
    <button id="addStaff" class="btn btn-success">Thêm nhân viên</button>
</div>
<div class="table-title text-center">
    <h4>Danh sách nhân viên</h4>
    <h5 id="success">{{$success ?? ''}}<h5>
    <h5 id="fail">{{$errorAdd ?? ''}}<h5>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Họ & tên</th>
            <th class="text-left">Chức vụ</th>
            <th class="text-left">Ca làm</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('profileStaff','ID='.$da['staffId'])}}">{{ $da['staffId']}}</a></td>
            <td class="text-left"><a href="">{{ $da['name']}}</a></td>
            @if($da['position'] == '0')
            <td class="text-left"><a href="">Quản lý</a></td>
            @else
            <td class="text-left"><a href="">Nhân viên</a></td>
            @endif
            
            <td class="text-left"><a href="">{{ $da['shift']}}</a></td>
        </tr>
@endforeach
        
    </tbody>
</table>

<!-- dialog -->
<div id="myModal" class="modal fade" role="dialog">
	    <div class="modal-dialog">

	        <!-- Modal content-->
	        <div class="modal-content">
	            <div class="modal-header">
					<h4 class="modal-title">Thêm nhân viên</h4>
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	            </div>
	            <div class="modal-body">
					<form onsubmit="return checksb()" action="{{route('addStaff')}}" method="get" style="display:inline;" enctype="multipart/form-data">
	                	{{csrf_field()}}
	                	<table id="Modal">
	                		<tr>
	                			<td class="tbModal">Id nhân viên:</td>
								<td class="tbModal"><input type="text" id="ID" name="ID"></input></td>
	                		</tr>
                            <tr> 
	                			<td class="tbModal">Tên nhân viên:</td>
	                			<td class="tbModal"><input type="text" id="name" name="name"></input></td>
	                		</tr>
							<tr>
								<td class="tbModal">Giới tính</td>
	                			<td class="tbModal">
									<select id="gender" name="gender">
										<option value="0">Nam</option>
										<option value="1">Nữ</option>
									</select>
								</td>
	                		</tr>
	                		
	                		<tr>
	                			<td class="tbModal">Ngày sinh</td>
	                			<td class="tbModal"><input type="date" id="birth" name="birth"></input></td>
	                		</tr>
	                		<tr>
	                			<td class="tbModal">Địa chỉ:</td>
	                			<td class="tbModal"><input type="text" id="address" name="address"></input></td>
	                		</tr>
							<tr>
	                			<td class="tbModal">Số điện thoại</td>
	                			<td class="tbModal"><input type="text" id="phone" name="phone"></input></td>
							</tr>
                            <tr>
	                			<td class="tbModal">Email</td>
	                			<td class="tbModal"><input type="email" id="email" name="email"></input></td>
							</tr>
							<tr>
	                			<td class="tbModal">Chức vụ</td>
	                			<td>
                                    <select id="position" name="position">
										<option value="0">Quản lý</option>
										<option value="1">Nhân viên</option>
                                    </select>
                                </td>
	                		</tr>
                            <tr>
	                			<td class="tbModal">Lương</td>
	                			<td class="tbModal"><input type="text" id="salery" name="salery"></input></td>
	                		</tr>
                            <tr>
	                			<td class="tbModal">Ca làm</td>
	                			<td class="tbModal"><input type="text" id="shift" name="shift"></input></td>
	                		</tr>

                        </table>
						</br>
	                	<p class="text-center" id="error" style="color:red;"></p>
						<div class="modal-footer">
							<button type="submit" id="add" class="btn btn-success">Thêm</button>
							<button type="button" onclick="reset()" class="btn btn-basic">Mới</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>                
						</div>
	                </form>
	            </div>
	            

	        </div>
	    </div>
	</div>
    <script src="js/staffMN.js"></script>

@endsection