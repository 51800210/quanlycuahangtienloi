@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/avt.png')}}">
    <h3>{{$da['staffId']}}- {{$da['name']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td id='ID' class="text-left">{{$da['staffId']}}</td>
        </tr>
        <tr>
            <td class="text-left">Họ & tên</td>
            <td id='name' class="text-left">{{$da['name']}}</td>
        </tr>
        <tr>
            <td class="text-left">Giới tính</td>
            @if($da['gender'] == '0')
            <td id='gender' class="text-left">Nam</td>
            @else
            <td id='gender' class="text-left">Nữ</td>
            @endif
        </tr>
        <tr>
            <td class="text-left">Ngày sinh</td>
            <td id='birth' class="text-left">{{$da['birth']}}</td>
        </tr>
        <tr>
            <td class="text-left">Địa chỉ</td>
            <td class="text-left"><input id='address' type="text" name="salery" value="{{$da['address']}}"></td>
        </tr>
        <tr>
            <td class="text-left">Số điện thoại</td>
            <td class="text-left"><input id='phone' type="text" name="salery" value="{{$da['phone']}}"></td>
        </tr>
        <tr>
            <td class="text-left">Email</td>
            <td class="text-left"><input id='email' type="text" name="salery" value="{{$da['email']}}"></td>
        </tr>
        <tr>
            <td class="text-left">Chức vụ</td>
        @if($da['position'] == '0')
            <td id="position" class="text-left">
                Quản lý
            </td>
        @else
            <td id="position" class="text-left">
                Nhân viên
            </td>
        @endif

        </tr>
        <tr>
            <td class="text-left">Lương</td>
            <td id="salery" class="text-left">{{$da['salery']}}</td>
        </tr>
        <tr>
            <td class="text-left">Ca làm việc</td>
            <td id="shift" class="text-left">{{$da['shift']}}</td>
        </tr>
    </tbody>
</table>
<div class="table-title text-center">
    <button id="save" class="btn btn-success">Lưu lại</button>
</div>
<div id='mess' class='text-center'></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/individual_Info.js"></script>
@endforeach
@endsection