@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/sale.png')}}">
    <h3>{{$da['ID']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td id='ID' class="text-left">{{$da['ID']}}</td>
        </tr>
        <tr>
            <td class="text-left">ID sản phẩm</td>
            <td class="text-left"><input id='idPr' type="text" name="idPr" value="{{$da['idPr']}}"></td>
        </tr>
        <tr>
            <td class="text-left">Khuyến mãi</td>
            <td class="text-left"><input id='sale' type="text" name="sale" value="{{$da['sale']}}"></td>
        </tr>
        
    </tbody>
</table>

<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>

<div class="table-title text-center"> 
    <button id="save" class="btn btn-success">Lưu lại</button>
    <button id="delete" class="btn btn-danger"><a href="{{route('deleteSale','ID='.$da['ID'])}}">Xóa</a></button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/individual_Sale.js"></script>
@endforeach
@endsection