@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/staffMN.css">

<div id="search">
    <form action="findSale" method="get">
        <input type="text" class="txt" name="ID" id="txtSearch" placeholder="Nhập ID" style="width: 250px;" />
        <button type="submit">Tìm kiếm</button>
    </form>
    <h6>{{$error ??''}}</h6>
</div>
<div class="table-title text-center">
    <h4>Danh sách khuyến mãi</h4>
    <h6>{{$success ??''}}</h6>
</div>
<table id="list" class="table-fill">
    <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">ID sản phẩm</th>
            <th class="text-left">Khuyễn mãi</th>
        </tr>
    </thead>
    <tbody class="table-hover">
@foreach ($data as $da)
        <tr>
            <td class="text-left"><a href="{{route('individual_Sale','ID='.$da['ID'])}}">{{$da['ID']}}</a></td>
            <td class="text-left"><a href="#">{{$da['idPr']}}</a></td>
            <td class="text-left"><a href="#">{{$da['sale']}}</a></td>
        </tr>
@endforeach         
    </tbody>
</table>



<script src="js/listSale.js"></script>

@endsection