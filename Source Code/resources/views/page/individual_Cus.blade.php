@extends('master.AdInterface')
@section('content')
<link rel="stylesheet" href="css/individual_Info.css">
<div class="table-title text-center">
@foreach($data as $da)
    <img id='img' src="{{asset('img/avt.png')}}">
    <h3>{{$da['ID']}}- {{$da['name']}}</h3>
</div>
<table id="list" class="table-fill">
    <tbody class="table-hover">
        <tr>
            <td class="text-left">ID</td>
            <td id='ID' class="text-left">{{$da['ID']}}</td>
        </tr>
        <tr>
            <td class="text-left">Họ & tên</td>
            <td id='name' class="text-left">{{$da['name']}}</td>
        </tr>
        <tr>
            <td class="text-left">Giới tính</td>
            @if($da['gender'] == '0')
            <td id='gender' class="text-left">Nam</td>
            @else
            <td id='gender' class="text-left">Nữ</td>
            @endif
        </tr>
        <tr>
            <td class="text-left">Địa chỉ</td>
            <td class="text-left"><input id='address' type="text" name="salery" value="{{$da['address']}}"></td>
        </tr>
        <tr>
            <td class="text-left">Số điện thoại</td>
            <td class="text-left"><input id='phone' type="text" name="salery" value="{{$da['phone']}}"></td>
        </tr>

        <tr>
            <td class="text-left">CMND</td>
            <td id="salery" class="text-left">{{$da['CMND']}}</td>
        </tr>
        <tr>
            <td class="text-left">Điểm tích lũy</td>
            <td id="salery" class="text-left">{{$da['point']}}</td>
        </tr>
    </tbody>
</table>

<div id='mess' class='text-center'></div>
<div id='error' class='text-center'></div>

<div class="table-title text-center">
    <button id="save" class="btn btn-success">Lưu lại</button>
    <button id="resetPoint" class="btn btn-basic">Xóa điểm</button>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/individual_Cus.js"></script>
@endforeach
@endsection