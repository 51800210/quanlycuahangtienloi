<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cửa hàng tiện lợi</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="css/AUI.css">
    


    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/UI.js"></script>    

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">

                <h3> <img src="img/logo.png"></h3>
                <strong><img src="img/user.png"></strong>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="{{route('UI')}}">
                        <i class="fas fa-receipt"></i>
                        Lập hóa đơn
                    </a>
                </li>
                <li>
                    <a href="{{route('listBillDone')}}">
                        <i class="fa fa-money" aria-hidden="true"></i>
                        Danh sách hóa đơn
                    </a>
                </li>
                <li>
                    <a href="#spSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-boxes"></i>
                        Sản phẩm bán
                    </a>
                    <ul class="collapse list-unstyled" id="spSubmenu">
                        <li>
                            <a href="{{route('listPrStore')}}">Danh sách sản phẩm bán</a>
                        </li>
                        <li>
                            <a href="{{route('addPrStore')}}">Thêm sản phẩm bán</a>
                        </li>

                        <li>
                            <a href="{{route('listPrHH')}}">Sản phẩm hết hạn</a>
                        </li>

                        <li>
                            <a href="{{route('addPrHH')}}">Thêm sản phẩm hết hạn</a>
                        </li>
                    </ul>
                </li>
                {!!$KM ?? ''!!}
                {!!$NV ?? ''!!}
                {!!$QLK ?? ''!!}
                <li> 
                    <a href="#khSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-handshake"></i>
                        Khách hàng
                    </a>
                    <ul class="collapse list-unstyled" id="khSubmenu">
                        <li>
                            <a href="{{route('listCus')}}">Danh sách khách hàng</a>
                        </li>
                        <li>
                            <a href="{{route('addCus')}}">Thêm khách hàng</a>
                        </li>
                    </ul>
                </li>
                {!!$TC ?? ''!!}
                <li> 
                    <a href="#TTCNSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-user"></i>
                        Thông tin cá nhân
                    </a>
                    <ul class="collapse list-unstyled" id="TTCNSubmenu">
                        <li>
                            <a href="{{route('listAnnou')}}">Thông báo</a>
                        </li>
                        <li>
                            <a href="{{route('individual_Info')}}">Xem hồ sơ</a>
                        </li>
                        <li>
                            <a href="{{route('changePass')}}">Thay đổi mật khẩu</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('logout')}}">
                        <i class="fas fa-sign-out-alt"></i>
                        Đăng xuất
                    </a>
                </li>
            </ul>
        </nav>
        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info" onclick="myFunction()">
                        <i class="fas fa-align-left"></i>
                        <span id="toggle" >Thu gọn</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link">Xin chào , {{$name}}</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- @@@@ -->
            @yield('content')
        </div>
    </div>


</body>

</html>