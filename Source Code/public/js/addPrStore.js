$(document).ready(function(){
    var random = Math.floor((Math.random() * 10000000) + 1);
    var d = new Date();
    var date = d.getDate() + '/' + (d.getMonth()+1) + '/'+ d.getFullYear(); 

    $('#ID').text('KH'+random);
    $('#save').click(function(){
        var ID = $('#ID').val();
        var qty = $('#qty').val();

        if(ID == '' || qty == ''){
            $('#error').text('Thiếu thông tin');
            return false;
        }
        if(qty == 0){
            $('#error').text('Số lượng sản phẩm không thể bằng 0');
            return false;
        }

        $.get('addPr2Store',
                        {ID:ID,
                        qty:qty,
                        date:date},
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){
                            if(datas.code == 1){
                                $('#error').text('');
                                $('#mess').text(datas.mess);
                            }
                            else{
                                $('#error').text(datas.mess);
                                $('#mess').text('');
                            }               
                        }
                    });
    });

    $('#new').click(function(){
        window.location.reload();
    });
});