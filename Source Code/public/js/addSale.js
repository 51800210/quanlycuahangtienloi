$(document).ready(function(){
    var random = Math.floor((Math.random() * 10000000) + 1);
    $('#ID').text('KM'+random);

    $('#save').click(function(){

        var sale = $('#sale').val();
        var ID = $('#ID').text();
        var idPr = $('#idPr').val();

        if(sale == '' || ID == ''){
            $('#error').text('Thiếu thông tin');
            return false;
        }


        $.get('add2Sale',
                        {ID:ID,
                        idPr:idPr,
                        sale:sale},
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){
                            if(datas.code == 1){
                                $('#error').text('');
                                $('#mess').text(datas.mess);
                            }
                            else{
                                $('#error').text(datas.mess);
                                $('#mess').text('');
                            }                            
                        }
                    });
    });

    $('#new').click(function(){
        window.location.reload();
    });
});