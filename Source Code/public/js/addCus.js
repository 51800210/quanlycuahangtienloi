$(document).ready(function(){
    var random = Math.floor((Math.random() * 10000000) + 1);
    $('#ID').text('KH'+random);
    $('#save').click(function(){
        var ID = $('#ID').text();
        var name = $('#name').val();

        var gender = $('#gender').val();

        var CMND = $('#CMND').val();
        var address = $('#address').val();
        var phone = $('#phone').val();

        if(gender == '' || name == '' || CMND == '' || address == '' || phone == ''){
            $('#error').text('Thiếu thông tin');
            return false;
        }
        $.get('addCus2db',
                        {ID:ID,
                        name:name,
                        gender:gender,
                        address:address,
                        phone:phone,
                        CMND:CMND},
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){
                            $('#error').text('');
                            $('#mess').text(datas);                               
                        }
                    });
    });

    $('#new').click(function(){
        window.location.reload();
    });
});