$(document).ready(function(){
    $('#save').click(function(){
        var ID = $('#ID').text();

        var address = $('#address').val();
        var phone = $('#phone').val();
        if(address == '' || phone == ''){
            $('#error').text('Thiếu thông tin');
            return false;
        }
        $.get('updateCus',
                        {ID:ID,
                        address:address,
                        phone:phone},
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){
                            $('#error').text('');
                            $('#mess').text(datas);                               
                        }
                    });
    });

    $('#resetPoint').click(function(){
        var ID = $('#ID').text();
        $.get('resetPoint',
                        {ID:ID},
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){                              
                        }
                    });

        window.location.reload();            
    });
});