$(document).ready(function () {
    $("#addPr").click(function () {
        $('#myModal').modal({
            backdrop:'static',  
            keyboard: false   
        });
    });

    var d = new Date();
    var date = d.getDate() + '/' + (d.getMonth()+1) + '/'+ d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':'+ d.getSeconds();
    $("#date").val(date);  
});

function checksub() {
    var ID = document.getElementById('ID').value;
    var name = document.getElementById('name').value;
    var date = document.getElementById('date').value;
    var type = document.getElementById('type').value;
    var price = document.getElementById('price').value;
    var qty = document.getElementById('qty').value;
    var HSD = document.getElementById('HSD').value;
    var NCC = document.getElementById('NCC').value;

    if(ID == ''){
        document.getElementById('ID').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập ID sản phẩm';
        return false;
    }
    if(name == ''){
        document.getElementById('name').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập tên sản phẩm';
        return false;
    }
    if(price == ''){
        document.getElementById('price').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập giá sản phẩm';
        return false;
    }    
    if(qty == ''){
        document.getElementById('qty').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập số lượng sản phẩm';
        return false;
    }
    if(HSD == ''){
        document.getElementById('HSD').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập hạn sử dụng sản phẩm';
        return false;
    }
    if(NCC == ''){
        document.getElementById('NCC').focus();
        document.getElementById('errorSign').innerHTML = 'Nhập nhà cung cấp';
        return false;
    }

    return true;
}

function reset(){
    document.getElementById('ID').value = '';
    document.getElementById('name').value = '';
    document.getElementById('price').value = '';
    document.getElementById('qty').value = '';
    document.getElementById('HSD').value = '';
    document.getElementById('errorSign').innerHTML = '';
}