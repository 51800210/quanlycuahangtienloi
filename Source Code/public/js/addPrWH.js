$(document).ready(function(){
    var d = new Date();
    var date = d.getDate() + '/' + (d.getMonth()+1) + '/'+ d.getFullYear();
    $("#date").val(date);  

    $('#save').click(function(){
        var date = $('#date').val();
        var ID = $('#ID').val();
        var name = $('#name').val();
        var qty = $('#qty').val();
        var type = $('#type').val();

        var price = $('#price').val();
        var priceInput = $('#priceInput').val();
        var NCC = $('#NCC').val();
        var HSD = $('#HSD').val();

        if(date == '' || ID == '' || name == '' || qty == '' || type == '' || price == '' || priceInput == '' || NCC == '' || HSD == ''){
            $('#error').text('Thiếu thông tin');
            return false;
        }
        if(qty == 0){
            $('#error').text('Số lượng phải lớn hơn 0');
            return false;
        }

        $.get('addPr2HW',
            {ID:ID,
            name:name,
            qty:qty,
            type:type,
            price:price,
            priceInput:priceInput,
            HSD:HSD,
            NCC:NCC,
            date:date},
            function(datas, status, ajax){
                if(ajax.status === 200 && ajax.readyState === 4){
                    if(datas.code == 1){
                        $('#error').text('');
                        $('#mess').text(datas.mess);
                    }
                    else{
                        $('#error').text(datas.mess);
                        $('#mess').text('');
                    }                            
                }
            });
    });

    $('#new').click(function(){
        window.location.reload();
    });
});