$(document).ready(function(){
    $('#save').click(function(){

        var pass = $('#pass').val();
        var passOld = $('#passOld').val();
        var passNew = $('#passNew').val();
        if(passOld =='' || passNew == ''){
            $('#error').text('Nhập mật khẩu đầy đủ');
            return false;
        }
        
        $.get('PrchangePass',
                        {passOld:passOld,
                        passNew:passNew,
                        pass:pass
                        },
                        function(datas, status, ajax){
                        if(ajax.status === 200 && ajax.readyState === 4){
                            if(datas == 1){
                                $('#error').text('');
                                $('#mess').text('Đổi mật khẩu thành công');
                            }
                            else 
                                $('#error').text('Nhập lại mật khẩu');                             
                        }
        });
    });
});