$(document).ready(function(){
    $('#shwOut').click(function(){
        $('#tb-inHW').css('display','none');
        $('#inHW').css('display','none');
        $('#sumIn').css('display','none');

        $('#tb-HH').css('display','none');
        $('#HH').css('display','none');
        $('#sumHH').css('display','none');

        $('#tb-Bill').css('display','none');
        $('#Bill').css('display','none');
        $('#sumBill').css('display','none');

        $('#tb-outHW').css('display','table');
        $('#outHW').css('display','block');
        $('#sumOut').css('display','block');

    });

    $('#shwIn').click(function(){
        $('#tb-inHW').css('display','table');
        $('#inHW').css('display','block');
        $('#sumIn').css('display','block');

        $('#tb-Bill').css('display','none');
        $('#Bill').css('display','none');
        $('#sumBill').css('display','none');

        $('#tb-HH').css('display','none');
        $('#HH').css('display','none');
        $('#sumHH').css('display','none');

        $('#tb-outHW').css('display','none');
        $('#outHW').css('display','none');
        $('#sumOut').css('display','none');
    });

    $('#shwHH').click(function(){
        $('#tb-HH').css('display','table');
        $('#HH').css('display','block');
        $('#sumHH').css('display','block');

        $('#tb-Bill').css('display','none');
        $('#Bill').css('display','none');
        $('#sumBill').css('display','none');
        
        $('#tb-inHW').css('display','none');
        $('#inHW').css('display','none');
        $('#sumIn').css('display','none');

        $('#tb-outHW').css('display','none');
        $('#outHW').css('display','none');
        $('#sumOut').css('display','none');
    });

    $('#shwBill').click(function(){
        $('#tb-Bill').css('display','table');
        $('#Bill').css('display','block');
        $('#sumBill').css('display','block');

        $('#tb-inHW').css('display','none');
        $('#inHW').css('display','none');
        $('#sumIn').css('display','none');

        $('#tb-outHW').css('display','none');
        $('#outHW').css('display','none');
        $('#sumOut').css('display','none');

        $('#tb-HH').css('display','none');
        $('#HH').css('display','none');
        $('#sumHH').css('display','none');
    });
});