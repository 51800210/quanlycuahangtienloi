<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Bill extends Model
{
    protected $collection = 'Bill';
    public $timestamps = false;
}
