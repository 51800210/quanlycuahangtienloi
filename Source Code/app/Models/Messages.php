<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;


class Messages extends Model
{
    protected $collection = "Messages";
    public $timestamps = false;
}
