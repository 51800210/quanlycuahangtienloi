<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class product extends Model
{
    protected $collection = "product";
    public $timestamps = false;
}
