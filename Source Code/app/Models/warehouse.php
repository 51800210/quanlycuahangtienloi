<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class warehouse extends Model
{
    protected $collection = "warehouse";
    public $timestamps = false;
}
