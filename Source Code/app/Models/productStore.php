<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class productStore extends Model
{
    protected $collection = "productStore";
    public $timestamps = false;

}
