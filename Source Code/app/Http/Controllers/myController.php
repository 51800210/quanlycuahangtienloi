<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Employee;
use App\Models\Bill;
use App\Models\product;
use App\Models\productStore;
use App\Models\warehouse;
use App\Models\Customer;
use App\Models\Sale;
use App\Models\productHH;
use App\Models\productOut;
use App\Models\Messages;
use Session;

class myController extends Controller
{
    public function login(Request $request) {
        return view("page.login",['err'=>$request['err']]);
    }

    public function UI(Request $request){
        if(session()->get('position') == '0')
            return view("page.invoice",['success'=> $request->success,
                                        'error'=> $request->error,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                            'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-database"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.invoice",['error'=> $request->error,'success'=> $request->success,'name'=>session()->get('name')]);
    }

    public function processLogin(){
        return redirect()->route('UI');
    }

    public function logout() {
        session()->flush();
        return redirect()->route('login');
    }

    public function getBill_Api(Request $request){
        $coll['sp'] = productStore::where('ID',$request->idPr)->get()->toArray();
        $coll['discount'] = Sale::select('sale')->where('idPr',$request->idPr)->get()->toArray();
        return response()->json($coll);
    }
    
    public function addBill(Request $request){
        if($request->Cus != ''){
            $checkCustomer = Customer::where('ID',$request->Cus)->get()->toArray();
            if(count($checkCustomer) == 0)
                return redirect()->route('UI',['error'=>'Sai mã khách hàng']);
        }
        
        $count = $request['count'];
        $coll = new Bill;
        $coll->idBill = $request['idBill'];
        $coll->date = $request['date'];
        $coll->staff = session()->get('staffId');
        $coll->PT = $request['PT'];
        $coll->sumFinal = $request['sumFinal'];

        $info = [];
        $i = 0;
        $countSp = 0;
        for($i; $i < $count; $i++){
            if($request->has('ID'.$i)){
                $check = productStore::where('ID', $request['ID'.$i])->get()->toArray();
                if(count($check) == 0)
                    return redirect()->route('UI',['error'=>'Không thành công']);
                if($request['qt'.$i] > $check[0]['qty'])
                    return redirect()->route('UI',['error'=>'Không đủ sản phẩm mã '.$request['ID'.$i]]);
                $info['sp'.$countSp] = ["ID" => $request['ID'.$i],
                            "productName" => $request['productName'.$i],
                            "cost" => $request['cost'.$i],
                            "qt" => $request['qt'.$i],
                            "discount" => $request['discount'.$i],
                            "sum" => $request['sum'.$i]
                            ];
                $qtyOld = $check[0]['qty'] - $request['qt'.$i];
                productStore::where('ID', $request['ID'.$i])->update(['qty'=>$qtyOld]);
                $countSp++;
            }

        }
        $coll->info = $info;
        $coll -> save();
        if($request->Cus != ''){
            $Cus = Customer::where('ID', $request->Cus)->get();
            $cusP = $Cus[0]['point'] + $request['sumFinal'];
            $idCus = Customer::where('ID', $request->Cus)->update(['point'=>$cusP]);
        }

        return redirect()->route('UI',['success'=>'Lưu hóa đơn thành công.']);

    }

    public function listProduct(Request $request){
        $coll = warehouse::get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listProduct",['error'=>$request->error,
                                            'success'=> $request->success,
                                            'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'data'=>$coll
            ]);    
        return redirect()->route('UI');
    }



/*     public function addPr(Request $request){
        $coll = new warehouse;
        $coll->ID = $request->ID;
        $coll->type = $request->type;
        $coll->name = $request->name;
        $coll->price = $request->price;
        $coll->qty = $request->qty;
        $coll->date = $request->date;
        $coll->HSD = $request->HSD;
        $coll->NCC = $request->NCC;

        $coll->save();
        return redirect()->route('listProduct',['success'=>'Lưu hóa đơn thành công.']);
    }
 */
    public function staffMN(Request $request){
        if(session()->get('position') == '0'){
            $data = Employee::orderBy('position','asc')->get()->toArray();
            return view("page.staffMN",['error'=>$request->error,
                                        'data'=>$data,
                                        'errorAdd'=>$request->errorAdd,
                                        'success'=>$request->success,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        }
        return redirect()->route('UI');;
    }

    public function addStaff(Request $request){
        if($request->ID == '' || $request->address == '' || $request->name == '' || $request->birth == '' || $request->phone == '' || $request->shift == '' || $request->salery == '')
            return redirect()->route('staffMN',['errorAdd'=>'Thiếu thông tin']);
        
        $check = Employee::where('staffId', $request->ID)->get();
        if(count($check) == 1)
            return redirect()->route('staffMN',['errorAdd'=>'Đã tồn tại id: '.$request->ID]);

        $coll = new Employee;
        $date = substr($request->birth,-2);
        $month = substr($request->birth,5,2);
        $year = substr($request->birth,0,4);
        $bir = $date."/".$month."/".$year;

        $coll->staffId = $request->ID;
        $coll->name = $request->name;
        $coll->gender = $request->gender;
        $coll->birth = $bir;
        $coll->address = $request->address;
        $coll->phone = $request->phone;
        $coll->email = $request->email;
        $coll->position = $request->position;
        $coll->salery = $request->salery;
        $coll->pass = md5('CNPM'.$request->ID);
        $coll->shift = $request->shift;
        $coll->save();

        return redirect()->route('staffMN',['success'=>'Đã thêm thành công nhân viên id: '.$request->ID]);

    }

    public function profileStaff(Request $request)
    {
        $data = Employee::where('staffId',$request->ID)->get()->toArray();
        
        if(session()->get('position') == '0'){
            return view('page.profileStaff',['data'=>$data,
                                            'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            
            ]);
        }

        else
            return redirect()->route('IU');
    }

    public function updateStaff(Request $request){
        if($request->has('position')){
            Employee::where('staffId',$request->ID)->update(['name'=>$request->name,
                                                        'gender'=>$request->gender,
                                                        'birth'=>$request->birth,
                                                        'address'=>$request->address,
                                                        'phone'=>$request->phone,
                                                        'email'=>$request->email,
                                                        'position'=>$request->position,
                                                        'salery'=>$request->salery,
                                                        'shift'=>$request->shift
                                                        ]);
            return response()->json("Lưu lại thành công");    
        }
        Employee::where('staffId',$request->ID)->update(['name'=>$request->name,
                                                        'gender'=>$request->gender,
                                                        'birth'=>$request->birth,
                                                        'address'=>$request->address,
                                                        'phone'=>$request->phone,
                                                        'email'=>$request->email,
                                                        'salery'=>$request->salery,
                                                        'shift'=>$request->shift
                                                        ]);

        return response()->json("Lưu lại thành công");
 
    }

    public function individual_Info(){
        $data = Employee::where('staffId', session()->get('staffId'))->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.individual_Info",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.individual_Info",['data'=>$data,'name'=>session()->get('name')]);
    }

    public function changePass(){
        $data = Employee::where('staffId', session()->get('staffId'))->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.changePass",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.changePass",['data'=>$data,'name'=>session()->get('name')]);
    
    }

    public function PrchangePass(Request $request)
    {
        if(md5($request->passOld) == $request->pass){
            Employee::where('staffId',session()->get('staffId'))->update(['pass'=>md5($request->passNew)]);
            return 1;
        }
            
        return 0;
    }

    public function deleteStaff(Request $request){
        Employee::where('staffId',$request->staffId)->delete();
        return redirect()->route('staffMN',['success'=>'Đã xóa nhân viên '.$request->staffId]);
    }

    public function listCus(Request $request){
        $data = Customer::get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listCus",['error'=>$request->error,
                                        'data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.listCus",['error'=>$request->error,'data'=>$data, 'name'=>session()->get('name')]);
    }

    public function addCus(){
        if(session()->get('position') == '0')
            return view("page.addCus",['name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.addCus",['name'=>session()->get('name')]);
    }

    public function addCus2db(Request $request){
        $cus = Customer::where('ID',$request->ID)->get()->toArray();

        if(count($cus) != 0)
            return 'Tài khoản đã tồn tại, vui lòng làm mới';

        $coll = new Customer;
        $coll->ID = $request->ID;
        $coll->name = $request->name;
        $coll->gender = $request->gender;
        $coll->address = $request->address;
        $coll->phone = $request->phone;
        $coll->CMND = $request->CMND;
        $coll->point = 0;
        $coll->save();
        return 'tạo tài khoản thành công';
    }

    public function individual_Cus(Request $request){
        $data = Customer::where('ID',$request->ID)->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.individual_Cus",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.individual_Cus",['data'=>$data,'name'=>session()->get('name')]);
    }

    public function updateCus(Request $request){
        Customer::where('ID',$request->ID)->update(['address'=>$request->address,
                                                    'phone'=>$request->phone]);
        return 'Cập nhật thông tin thành công';
    }

    public function listSale(Request $request){
        $data = Sale::get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listSale",['error'=>$request->error,
                                        'success'=>$request->success,
                                        'data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function addSale(){
        if(session()->get('position') == '0')
            return view("page.addSale",['name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function add2Sale(Request $request){
        $idPr = productStore::where('ID', $request->idPr)->get()->toArray();
        if(count($idPr) == 0){
            $mess=['code'=>0,'mess'=>'Sai mã sản phẩm'];
            return $mess;
        }
            
        $idSale = Sale::where('ID', $request->ID)->get()->toArray();
        if(count($idSale) != 0){
            $mess=['code'=>0,'mess'=>'Vui lòng thử lại'];
            return $mess;
        }

        if($request->sale > 100){
            $mess=['code'=>0,'mess'=>'Khuyến mãi không vượt quá 100'];
            return $mess;
        }

        $checkSale = Sale::where('idPr', $request->idPr)->get()->toArray();
        if(count($checkSale) != 0){
            Sale::where('idPr',$request->idPr)->update(['sale'=>$request->sale]);
            $mess=['code'=>1,'mess'=>'Cập nhập thành công'];
            return $mess;
        }

        $coll = new Sale;
        $coll->ID = $request->ID;
        $coll->idPr = $request->idPr;
        $coll->sale = $request->sale;
        $coll->staff_inPut = session()->get('staffId');

        $coll->save();

        $mess=['code'=>1,'mess'=>'Thêm thành công'];
        return $mess;
    }

    public function individual_Sale(Request $request){
        $data = Sale::where('ID',$request->ID)->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.individual_Sale",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function updateSale(Request $request){
        $idPr = productStore::where('ID', $request->idPr)->get()->toArray();
        if(count($idPr) == 0){
            $mess=['code'=>0,'mess'=>'Thử lại'];
            return $mess;
        }
        Sale::where('ID',$request->ID)->update(['idPr'=>$request->idPr, 'sale'=>$request->sale]);
        $mess=['code'=>1,'mess'=>'Lưu lại thành công'];
        return $mess;
    }

    public function addPrStore(){
        if(session()->get('position') == '0')
            return view("page.addPrStore",['name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.addPrStore",['name'=>session()->get('name')]);
    }
//xuat kho
    public function addPr2Store(Request $request){
        $idPr = warehouse::where('ID', $request->ID)->get()->toArray();
        if(count($idPr) == 0){
            $mess=['code'=>0,'mess'=>'Sai mã sản phẩm hoặc đã hết sản phẩm'];
            return $mess;
        }

        if($idPr[0]['qty'] < $request->qty){
            $mess=['code'=>0,'mess'=>'Số lượng hàng trong kho là: '.$idPr[0]['qty']];
            return $mess;
        }

        $check = productStore::where('ID', $request->ID)->get()->toArray();

        if(count($check) == 0){
            $coll = new productStore;
            $coll->ID = $idPr[0]['ID'];
            $coll->type = $idPr[0]['type'];
            $coll->name = $idPr[0]['name'];
            $coll->price = $idPr[0]['price'];
            $coll->priceInput = $idPr[0]['priceInput'];            
            $coll->qty = $request->qty;
            $coll->date = $request->date;
            $coll->HSD = $idPr[0]['HSD'];
            $coll->NCC = $idPr[0]['NCC'];
            $coll->staff_inPut = session()->get('staffId');
            $coll->save();
        }
        else{
            $qtyAdd = $check[0]['qty'] + $request->qty;
            productStore::where('ID', $request->ID)->update(['date'=>$request->date,'qty'=>$qtyAdd, 'staff_inPut'=>session()->get('staffId')]);
        }
        
        $row = new productOut;
        $row->ID = $idPr[0]['ID'];
        $row->type = $idPr[0]['type'];
        $row->name = $idPr[0]['name'];
        $row->price = $idPr[0]['price'];
        $row->qty = $request->qty;
        $row->date = $request->date;
        $row->NCC = $idPr[0]['NCC'];
        $row->AllPrice = $request->qty * $idPr[0]['price'];            
        $row->staff_inPut = session()->get('staffId');

        $row->save();

        
        $qtyAf = $idPr[0]['qty'] - $request->qty;
        warehouse::where('ID', $request->ID)->update(['qty'=>$qtyAf,'']);

        $idPrAfter = warehouse::where('ID', $request->ID)->get()->toArray();
        if($idPrAfter[0]['qty'] == 0){
            warehouse::where('ID', $request->ID)->delete();
        }

        $mess=['code'=>1,'mess'=>'Xuất kho thành công với số lượng là: '.$request->qty];
        return $mess;

    }

    public function listPrStore(Request $request){
        $data = productStore::orderBy('price','asc')->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listPrStore",['error'=>$request->error,
                                            'data'=>$data,
                                            'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);   
        return view("page.listPrStore",['error'=>$request->error,'data'=>$data,'name'=>session()->get('name')]);
        
    }

    public function individual_prStore(Request $request){
        $data = productStore::where('ID',$request->ID)->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.individual_prStore",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);  
        return view("page.individual_prStore",['data'=>$data,'name'=>session()->get('name')]); 
    }

    public function addPrHH(){
        if(session()->get('position') == '0')
            return view("page.addPrHH",['name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.addPrHH",['name'=>session()->get('name')]);
    }

    public function addPr2HH(Request $request){
        $idPr = productStore::where('ID', $request->ID)->get()->toArray();

        if(count($idPr) == 0){
            $mess=['code'=>0,'mess'=>'Sai mã sản phẩm'];
            return $mess;
        }

        $coll = new productHH;
        $coll->ID = $idPr[0]['ID'];
        $coll->type = $idPr[0]['type'];
        $coll->name = $idPr[0]['name'];
        $coll->price = $idPr[0]['price'];
        $coll->priceInput = $idPr[0]['priceInput'];
        $coll->qty = $idPr[0]['qty'];
        $coll->date = $idPr[0]['date'];
        $coll->HSD = $idPr[0]['HSD'];
        $coll->NCC = $idPr[0]['NCC'];
        $coll->staff_inPut = session()->get('staffId');
        $coll->save();

        productStore::where('ID', $request->ID)->delete();

        $mess=['code'=>1,'mess'=>'Thêm thành công'];
        return $mess;

    }

    public function listPrHH(){
        $data = productHH::get()->toArray();

        if(session()->get('position') == '0')
            return view("page.listPrHH",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.listPrHH",['data'=>$data,'name'=>session()->get('name')]);
    }

    public function resetPoint(Request $request){
        Customer::where('ID',$request->ID)->update(['point'=>0]);
    }

    public function addPrWH(){
        if(session()->get('position') == '0')
            return view("page.addPRWH",['name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function addPr2HW(Request $request){
        $check = warehouse::where('ID',$request->ID)->get()->toArray();

        if(count($check) != 0){
            $check2 = warehouse::where('ID',$request->ID)->where('name',$request->name)->get()->toArray();
            if(count($check2) == 0){
                $mess=['code'=>0,'mess'=>$request->ID.' không phải tên '.$request->name];
                return $mess;
            }
                
            $qtyAf = $check[0]['qty'] + $request->qty;
            warehouse::where('ID',$request->ID)->update(['qty'=>$qtyAf,'staff_inPut'=>session()->get('staffId'), 'date'=>$request->date,'price'=>$request->price]);
        }
        else{
            //add WH
            $coll = new warehouse;
            $coll->ID = $request->ID;
            $coll->type = $request->type;
            $coll->name = $request->name;
            $coll->price = $request->price;
            $coll->priceInput = $request->priceInput;
            $coll->qty = $request->qty;
            $coll->date = $request->date;
            $coll->HSD = $request->HSD;
            $coll->NCC = $request->NCC;
            $coll->staff_inPut = session()->get('staffId');

            $coll->save();
        }

        //add pr

        $row = new product;
        $row->ID = $request->ID;
        $row->type = $request->type;
        $row->name = $request->name;
        $row->priceInput = $request->priceInput;
        $row->qty = $request->qty;
        $row->date = $request->date;
        $row->NCC = $request->NCC;
        $row->AllPrice = $request->qty * $request->priceInput;
        $row->staff_inPut = session()->get('staffId');            

        $row->save();
        $mess=['code'=>1,'mess'=>'Nhập sản phẩm thành công sản phẩm mã '.$request->ID.' với só lượng '.$request->qty];
        return $mess;
    }

    public function individual_prHW(Request $request){
        $data = warehouse::where('ID',$request->ID)->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.individual_prHW",['data'=>$data,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function findCus(Request $request){
        $coll = Customer::where('ID',$request->ID)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('listCus',['error'=>'Không có khách hàng này']);
        return redirect()->route('individual_Cus',['ID'=>$request->ID]);
    }

    public function findStaff(Request $request){
        $coll = Employee::where('staffId',$request->ID)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('staffMN',['error'=>'Không có nhân viên']);
        return redirect()->route('profileStaff',['ID'=>$request->ID]);
    }

    public function findPrHW(Request $request){
        $coll = warehouse::where('ID',$request->ID)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('listProduct',['error'=>'Không sản phẩm này']);
        return redirect()->route('individual_prHW',['ID'=>$request->ID]);
    }

    public function findPrStore(Request $request){
        $coll = productStore::where('ID',$request->ID)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('listPrStore',['error'=>'Không sản phẩm này']);
        return redirect()->route('individual_prStore',['ID'=>$request->ID]);
    }

    public function findSale(Request $request){
        $coll = Sale::where('ID',$request->ID)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('listSale',['error'=>'Không sản phẩm này']);
        return redirect()->route('individual_Sale',['ID'=>$request->ID]);
    }

    public function listIO(){
        $data = product::orderBy('date', 'desc')->get()->toArray();
        $dataOut = productOut::orderBy('date', 'desc')->get()->toArray();
        $dataHH = productHH::orderBy('date', 'desc')->get()->toArray();
        $dataBill = Bill::orderBy('date', 'desc')->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listIO",['dataBill'=>$dataBill,
                                        'dataHH'=>$dataHH,
                                        'data'=>$data,
                                        'dataOut'=>$dataOut,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function profit(){
        $dataHH = productHH::get()->toArray();
        $dataStaff = Employee::get()->toArray();

        if(session()->get('position') == '0')
            return view("page.profit",['dataHH'=>$dataHH,
                                        'dataStaff'=>$dataStaff,
                                        'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function sent(Request $request){
        if(session()->get('position') == '0')
            return view("page.sent",['error'=>$request->error,
                                    'success'=>$request->success,
                                    'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return redirect()->route('UI');
    }

    public function prSent(Request $request){
        if($request->has('all')){
            $coll = Employee::get()->toArray();
            foreach($coll as $row){
                $db = new Messages;
                $db->idSent = session()->get('staffId');
                $db->nameSent = session()->get('name');
                $db->idReceive = $row['staffId'];
                $db->content = $request->content;
                $db->date = $request->date;

                $db->save();
            }
            return redirect()->route('sent',['success'=>'Đã gửi xong tin nhắn']);

        }
        $arr = explode(',',$request->staffId);
        for($i = 0; $i< sizeof($arr) ; $i++){
            $check = Employee::where('staffId', $arr[$i])->get()->toArray();
            if(count($check) == 0)
                return redirect()->route('sent',['error'=>'Không tồn tại nhân viên id: '.$arr[$i]]);
        }

        for($j = 0; $j< sizeof($arr) ; $j++){
            $db = new Messages;
            $db->idSent = session()->get('staffId');
            $db->nameSent = session()->get('name');
            $db->idReceive = $arr[$j];
            $db->content = $request->content;
            $db->date = $request->date;

            $db->save();
        }
        return redirect()->route('sent',['success'=>'Đã gửi xong tin nhắn']);

    }

    public function listAnnou(){
        $data = Messages::where('idReceive',session()->get('staffId'))->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listAnnou",['data'=>$data,
                                    'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.listAnnou",['data'=>$data, 'name'=>session()->get('name')]);

    }

    public function Annou(Request $request){
        $data = Messages::where('_id',$request->ID)->get()->toArray();
        if(session()->get('position') == '0')
            return view("page.Annou",['data'=>$data,
                                    'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.Annou",['data'=>$data, 'name'=>session()->get('name')]);
    }

    public function listBillDone(Request $request){
        $data = Bill::get()->toArray();
        if(session()->get('position') == '0')
            return view("page.listBillDone",['error'=>$request->error,
                                            'data'=>$data,
                                            'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.listBillDone",['error'=>$request->error,'data'=>$data,'name'=>session()->get('name')]);
    }

    public function individual_BillDone(Request $request){
        $data = Bill::where('idBill',$request->idBill)->get()->toArray();


/*         foreach($data as $da){
            foreach($da['info'] as $d)
                echo $d['ID'];
        } */
        if(session()->get('position') == '0')
            return view("page.individual_BillDone",['data'=>$data,
                                            'name'=>session()->get('name'),
                                            'NV'=>'<li> 
                                                        <a href="#nvSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="fas fa-address-card"></i>
                                                            Nhân viên
                                                        </a>
                                                        <ul class="collapse list-unstyled" id="nvSubmenu">
                                                            <li>
                                                                <a href="staffMN">Danh sách nhân viên</a>
                                                            </li>
                                                            <li>
                                                                <a href="sent">Gửi tin nhắn nhân viên</a>
                                                            </li>
                                                        </ul>
                                                    </li>',
                                                    'TC'=>'<li> 
                                                            <a href="#tcSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Tài chính
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="tcSubmenu">
                                                                <li>
                                                                    <a href="listIO">Thống kê sản phẩm</a>
                                                                </li>
                                                                <li>
                                                                    <a href="profit">Chi phí</a>
                                                                </li>
                                                            </ul>
                                                        </li>',
                                                        'KM'=>'<li> 
                                                    <a href="#KMSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                        <i class="fas fa-bullhorn"></i>
                                                        Khuyến mãi
                                                    </a>
                                                    <ul class="collapse list-unstyled" id="KMSubmenu">
                                                        <li>
                                                            <a href="listSale">Danh sách khuyến mãi</a>
                                                        </li>
                                                        <li>
                                                            <a href="addSale">Thêm khuyến mãi</a>
                                                        </li>
                                                    </ul>
                                                </li>',
                                                        'QLK'=>'<li> 
                                                            <a href="#kSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                                                <i class="fas fa-coins"></i>
                                                                Quản lý kho
                                                            </a>
                                                            <ul class="collapse list-unstyled" id="kSubmenu">
                                                                <li>
                                                                    <a href="addPrWH">Nhập hàng</a>
                                                                </li>
                                                                <li>
                                                                    <a href="listProduct">Danh sách sản phẩm kho</a>
                                                                </li>
                                                            </ul>
                                                        </li>'
            ]);    
        return view("page.individual_BillDone",['data'=>$data,'name'=>session()->get('name')]);
    }

    public function findBillDone(Request $request){
        $coll = Bill::where('idBill',$request->idBill)->get()->toArray();
        if(count($coll) == 0)
            return redirect()->route('listBillDone',['error'=>'Không có hóa đơn này']);
        return redirect()->route('individual_BillDone',['idBill'=>$request->idBill]);
    }

    public function deleteSale(Request $request){
        Sale::where('ID',$request->ID)->delete();
        return redirect()->route('listSale',['success'=>'Xóa thành công khuyến mãi: '. $request->ID]);

    }

    public function start(){
        $staffId = 'admin';
        $name = 'admin';
        $gender = 0;
        $birth = '01/01/1990';
        $address = 'CNPM';
        $phone = '0123456789';
        $email = 'admin@gmail.com';
        $position = 0;
        $salery = 0;
        $shift = '0-0';

        $check = Employee::where('staffId', $staffId)->get()->toArray();
        if(count($check) != 0)
            return redirect()->route('login',['err'=>'Đã tạo admin']);
        $coll = new Employee;

        $coll->staffId = $staffId;
        $coll->name = $name;
        $coll->gender = $gender;
        $coll->birth = $birth;
        $coll->address = $address;
        $coll->phone = $phone;
        $coll->email = $email;
        $coll->position = $position;
        $coll->salery = $salery;
        $coll->pass = md5('CNPM'.$staffId);
        $coll->shift = $shift;
        $coll->save();

        return redirect()->route('login',['err'=>'Đã tạo admin']);
    }
}
