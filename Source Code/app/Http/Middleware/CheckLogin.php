<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Employee;
use Session;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $staffId = $request['staffId'];
        $pass = md5($request['pass']);
        $document = Employee::where('staffId',$staffId)->where('pass',$pass)->get()->toArray();
        if(count($document) != 1)
            return redirect()->route('login',['err'=>'staff ID or password wrong!']);

        foreach($document as $doc){
            $name = $doc['name'];
            $position = $doc['position'];
        }
        session()->put('staffId', $staffId);
        session()->put('name', $name);
        session()->put('position', $position);
        return $next($request);
    }
}
