<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\myController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



route::group(['middleware' => ['web']], function(){
    Route::get('start', [myController::class, 'start'])->name('start')->middleware('CheckUSout');

    Route::get('', [myController::class, 'login'])->name('login')->middleware('CheckUSout');

    Route::get('UI', [myController::class, 'UI'])->name('UI')->middleware('CheckUS');
    
    Route::get('staffMN', [myController::class, 'staffMN'])->name('staffMN')->middleware('CheckUS');
  
    Route::get('addStaff', [myController::class, 'addStaff'])->name('addStaff')->middleware('CheckUS');

    Route::get('updateStaff', [myController::class, 'updateStaff'])->name('updateStaff')->middleware('CheckUS');

    Route::get('profileStaff', [myController::class, 'profileStaff'])->name('profileStaff')->middleware('CheckUS');

    Route::get('listProduct', [myController::class, 'listProduct'])->name('listProduct')->middleware('CheckUS');
    
    Route::get('addPrWH', [myController::class, 'addPrWH'])->name('addPrWH')->middleware('CheckUS');

    Route::get('individual_prHW', [myController::class, 'individual_prHW'])->name('individual_prHW')->middleware('CheckUS');

    Route::get('addPr2HW', [myController::class, 'addPr2HW'])->name('addPr2HW')->middleware('CheckUS');

    Route::get('individual_Info', [myController::class, 'individual_Info'])->name('individual_Info')->middleware('CheckUS');

    Route::get('deleteStaff', [myController::class, 'deleteStaff'])->name('deleteStaff')->middleware('CheckUS');

    Route::get('changePass', [myController::class, 'changePass'])->name('changePass')->middleware('CheckUS');

    Route::get('PrchangePass', [myController::class, 'PrchangePass'])->name('PrchangePass')->middleware('CheckUS');

    Route::post('processLogin', [myController::class, 'processLogin'])->name('processLogin')->middleware('CheckLogin');

    Route::get('logout', [myController::class, 'logout'])->name('logout');
    
    Route::get('addPr', [myController::class, 'addPr'])->name('addPr');

    route::get('getBill_Api',[myController::class, 'getBill_Api'])->name('getBill_Api');
    //route::get('test',[myController::class, 'test'])->name('test');

    route::post('addBill',[myController::class, 'addBill'])->name('addBill');

    route::get('listCus',[myController::class, 'listCus'])->name('listCus')->middleware('CheckUS');

    route::get('addCus',[myController::class, 'addCus'])->name('addCus')->middleware('CheckUS');

    route::get('addCus2db',[myController::class, 'addCus2db'])->name('addCus2db')->middleware('CheckUS');

    route::get('individual_Cus',[myController::class, 'individual_Cus'])->name('individual_Cus')->middleware('CheckUS');

    route::get('updateCus',[myController::class, 'updateCus'])->name('updateCus')->middleware('CheckUS');

    route::get('listSale',[myController::class, 'listSale'])->name('listSale')->middleware('CheckUS');

    route::get('addSale',[myController::class, 'addSale'])->name('addSale')->middleware('CheckUS');

    route::get('add2Sale',[myController::class, 'add2Sale'])->name('add2Sale')->middleware('CheckUS');

    route::get('individual_Sale',[myController::class, 'individual_Sale'])->name('individual_Sale')->middleware('CheckUS');

    route::get('updateSale',[myController::class, 'updateSale'])->name('updateSale')->middleware('CheckUS');
    
    route::get('listPrStore',[myController::class, 'listPrStore'])->name('listPrStore')->middleware('CheckUS');
    
    route::get('addPrStore',[myController::class, 'addPrStore'])->name('addPrStore')->middleware('CheckUS');
    
    route::get('addPr2Store',[myController::class, 'addPr2Store'])->name('addPr2Store')->middleware('CheckUS');

    route::get('listPrStore',[myController::class, 'listPrStore'])->name('listPrStore')->middleware('CheckUS');

    route::get('individual_prStore',[myController::class, 'individual_prStore'])->name('individual_prStore')->middleware('CheckUS');

    route::get('addPrHH',[myController::class, 'addPrHH'])->name('addPrHH')->middleware('CheckUS');

    route::get('addPr2HH',[myController::class, 'addPr2HH'])->name('addPr2HH')->middleware('CheckUS');

    route::get('listPrHH',[myController::class, 'listPrHH'])->name('listPrHH')->middleware('CheckUS');

    route::get('resetPoint',[myController::class, 'resetPoint'])->name('resetPoint')->middleware('CheckUS');

    route::get('findCus',[myController::class, 'findCus'])->name('findCus')->middleware('CheckUS');

    route::get('findStaff',[myController::class, 'findStaff'])->name('findStaff')->middleware('CheckUS');

    route::get('findPrHW',[myController::class, 'findPrHW'])->name('findPrHW')->middleware('CheckUS');

    route::get('findPrStore',[myController::class, 'findPrStore'])->name('findPrStore')->middleware('CheckUS');

    route::get('findBillDone',[myController::class, 'findBillDone'])->name('findBillDone')->middleware('CheckUS');

    route::get('findSale',[myController::class, 'findSale'])->name('findSale')->middleware('CheckUS');

    route::get('listIO',[myController::class, 'listIO'])->name('listIO')->middleware('CheckUS');

    route::get('profit',[myController::class, 'profit'])->name('profit')->middleware('CheckUS');

    route::get('sent',[myController::class, 'sent'])->name('sent')->middleware('CheckUS');

    route::get('prSent',[myController::class, 'prSent'])->name('prSent')->middleware('CheckUS');

    route::get('listAnnou',[myController::class, 'listAnnou'])->name('listAnnou')->middleware('CheckUS');

    route::get('Annou',[myController::class, 'Annou'])->name('Annou')->middleware('CheckUS');

    route::get('listBillDone',[myController::class, 'listBillDone'])->name('listBillDone')->middleware('CheckUS');

    route::get('individual_BillDone',[myController::class, 'individual_BillDone'])->name('individual_BillDone')->middleware('CheckUS');

    route::get('deleteSale',[myController::class, 'deleteSale'])->name('deleteSale')->middleware('CheckUS');

});
